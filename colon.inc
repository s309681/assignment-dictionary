%macro colon 2
    %ifid %2
        %ifstr %1
        ; сначала идёт ключ (вычисляем с помощью длинны строки + 1 (ноль-терминатор))
        ; затем метка следующего блока
        ; затем значение
        %ifdef NEXT
            %%next: dq NEXT
        %else
            %%next: dq 0
        %endif
        %define NEXT %%next
        .key: db %1, 0
        %2:
        ; здесь будет идти само значение после блока макроса, как показано в примере
        %else
            %fatal "Not a string"
        %endif
    %else
        %fatal "Invalid label"
    %endif
%endmacro

global find_word
extern string_equals
extern string_length

section .text
; rdi - указатель на строку в буфере
; rsi - указатель на слкдующий элемент списка


find_word:
    push rdi
    push rsi
    add rsi, 8
    call string_equals
    cmp rax, 1
    pop rsi
    pop rdi
    jz .complete
    mov rsi, [rsi]
    cmp rsi, 0
    je .end_of_list
    jmp find_word

    .end_of_list:
        xor rax, rax
        ret
    .complete:
        mov rax, rsi
        ret

%define MAX_LENGTH 256
global _start
%include "lib.inc"
%include "dict.inc"
%include "words.inc"
section .rodata
invalid_key_str: db 'you enter very long key', 0
nothing_found_str: db 'nothing found for this key', 0

section .text
_start:
    mov rdi, rsp
    mov r10, rsp
    sub rsp, MAX_LENGTH
    mov rsi, MAX_LENGTH
    call read_word
    cmp rax, 0
    jz .print_invalid_key_str
    mov rdi, r10
    mov rsi, NEXT

    call find_word
    cmp rax, 0
    jz .print_nothing_found_str
    mov rdi, rax
    add rdi, 8
    push rdi
    call string_length
    inc rax
    pop rdi
    add rdi, rax
    call print_string
    call print_newline
    mov rdi, 0
    call exit

    .print_invalid_key_str:
    mov rdi, invalid_key_str
    call print_err
    call print_newline
    mov rdi, 2
    call exit

    .print_nothing_found_str:
    mov rdi, nothing_found_str
    call print_err
    call print_newline
    mov rdi, 2
    call exit

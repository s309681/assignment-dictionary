main : lib.o main.o dict.o
	ld -g -o main lib.o main.o dict.o

%.o: %.asm
	nasm -f elf64 $< -o $@

.PHONY : clean	
clean : 
	rm *.o
